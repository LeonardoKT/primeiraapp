import os, time
Vet_numero_voo = [0, 1, 2, 3, 4, 5];
Vet_origem_voo = [" ", "1- Porto Alegre", "2- Erechim", "3- São Paulo", "4- Rio de Janeiro", "5- Brasilia"];
Vet_destino_voo = [" ", "1- Caribe", "2- Colorado", "3- Texas", "4- URSS", "5- Paris"];
Vet_lugares_vazio = [0, 1, 3, 8, 10, 7];
Num_voos = 5;

print('*' * 19);
print("---BEM  VINDO---");
print("*" * 19);

def Consultar ():
    print("----------Consultar por:----------")
    print("1- Por número de voo");
    print("2- Por Origem");
    print("3- Por destino");
    print("-" * 20);
    resp2 = int(input("Digite o número correspondente a operação: "));
    os.system("cls");

    if resp2 == 1:
        resp = int(input("Digite o numero do voo"));
        print("Voo número " + str(resp));
        print("Origem: " + Vet_origem_voo[resp]);
        print("Destino: " + Vet_destino_voo[resp]);
        print("Existem " + str(Vet_lugares_vazio[resp]) + " lugares livres");
        pergunta = int(input("Digite 1 para voltar ao menu e 0 para sair: "));
        os.system("cls")
        if pergunta == 1:
            Menu();
        if pergunta == 0:
            exit();

    elif resp2 == 2:
        print(("Origem dos voos:"));
        for x in Vet_origem_voo:
            print(x);
        resp = int(input("Digite o numero correspondente a origem do voo: "));
        print("Voo número " + str(resp));
        print("Origem: " + Vet_origem_voo[resp]);
        print("Destino: " + Vet_destino_voo[resp]);
        print("Existem " + str(Vet_lugares_vazio[resp]) + " lugares livres");
        pergunta = int(input("Digite 1 para voltar ao menu e 0 para sair: "));
        os.system("cls")
        if pergunta == 1:
            Menu();
        if pergunta == 0:
            exit();

    elif resp2 == 3:
        print("Destino dos vooos:");
        for x in Vet_destino_voo:
            print(x)
        resp = int(input("Digite o numero correspondente a origem do voo: "));
        print("Voo número " + str(resp));
        print("Origem: " + Vet_origem_voo[resp]);
        print("Destino: " + Vet_destino_voo[resp]);
        print("Existem " + str(Vet_lugares_vazio[resp]) + " lugares livres");
        pergunta = int(input("Digite 1 para voltar ao menu e 0 para sair: "));
        os.system("cls")
        if pergunta == 1:
            Menu();
        if pergunta == 0:
            exit();


def EfetuarReserva ():
    Num_voo = int(input("Digite o numero do voo: "));

    if Vet_lugares_vazio[Num_voo] < 12 and Vet_lugares_vazio[Num_voo] > 0:
        Vet_lugares_vazio[Num_voo] -=1;
        print("Reservado com sucesso!!");
        Menu();
    else:
        print("voo lotado ou inexistente");
        Menu();

def Menu ():
    print("-------Menu-------");
    print("1- Consultar");
    print("2- Efetuar reserva");
    print("3- Sair");
    print("------------------");
    resp = int(input("Digite o número correspondente a operação: "));
    os.system("cls");
    if resp == 1:
        Consultar();

    if resp == 2:
        EfetuarReserva();

    if resp == 3:
        exit();
Menu()
